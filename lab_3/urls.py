from django.urls import path
from .views import add_friend,index

urlpatterns = [
    path('', index, name='index3'),
    path('add', add_friend, name='addfriend'),
]