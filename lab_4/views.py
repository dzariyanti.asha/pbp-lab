from django.http.response import HttpResponseRedirect
from .forms import NoteForm
from lab_2.models import Note
from django.shortcuts import render

# Create your views here.
def index(request):
    pesan = Note.objects.all().values()
    response = {'pesan': pesan}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm()

    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()

        return HttpResponseRedirect('/lab-4')

    response = {'form': form}
    return render(request, 'lab4_form.html', response)

def note_list(request):
    pesan = Note.objects.all().values()
    response = {'pesan': pesan}
    return render(request, 'lab4_note_list.html', response)