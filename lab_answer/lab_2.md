1. Apakah perbedaan antara JSON dan XML?
- Interpretasi Bahasa
Perbedaan yang pertama dari XML dan JSON adalah interpretasi bahasanya, pada XML sebagai bahasa markup maka bahasa yang disajikan memang menujukkan nama tersebut, sedangkan JSON lebih ke perwakilan saja nama tersebut merupakan objek apa.
- Kompleksitas
Pada penulisan kode, JSON cenderung lebih sederhana ketimbang XML dan dalam pembacaan kodenya JSON lebih mudah untuk dibaca.
- Superset
Secara sederhana, kedua bahasa ini memiliki superset yang memang berbeda yaitu, XML yang merupakan subset dari SGML sebagai suatu bahasa markup dan JSON sebagai subset dari JavaSctipt sehingga lebih mudah dipahami.
- Encoding
Metode pengkodean karakter antara XML dan JSON tidak sama, sebab JSON memiliki encoding yang cukup terbatas yaitu hanya pada UTF-8 Encoding, sedangkan XML dapat menggunakan berbagai macam tipe encoding.
- Tag
XML yang memang lebih kompleks dan memerhatikan detail memerlukan tag pembuka dan penutup untuk tiap bagiannya sehingga berbeda dengan JSON yang tidak terlalu memerlukan tag penutup.

2. Apakah perbedaan antara HTML dan XML?
- Penggunaan Bahasa
Dalam penggunaan bahasanya, HTML dan XML yang notabenenya merupakan bahasa markup memiliki kegunaan yang berbeda. HTML sendiri digunakan untuk menampilkan data pada suatu webpage, sedangkan XML lebih ke menyimpan dan mengantarkan data.
- Tag
Pada XML, tag tidak ada ketentuan secara baku sehingga dapat secara bebas kita tentukan sendiri tag khusus tersebut. Untuk HTML, tag sudah tersedia dan cukup untuk menyesuaikan kegunaan dalam pemilihan tag tersebut.
- Tipe Bahasa
Kedua bahasa markup ini memiliki tipe bahasa yang cukup signifikan dalam case sensitivenya, dimana XML memperhitungkan kapitalisasi dalam tiap hurufnya dan HTML tidak demikian.
- Tampilan Data
Untuk melakukan penampilan data, HTML jelas dapat langsung menampilkan data dan memunculkannya dalam webpage. Hal ini berlainan dengan XML yang memerlukan pemrosesan data ataupun converter terlebih dahulu.
- Error
Perbedaan lainnya dari kedua bahasa ini adalah pada pelaksanaan errornya, HTML lebih tidak peka ketimbang XML sebab XML perlu memastikan tidak ada sedikit pun error yang terjadi dan hal ini berpengaruh pada penggunaan tag yang harus lengkap serta penempatan urutan yang benar. 
