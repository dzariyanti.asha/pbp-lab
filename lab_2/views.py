from .models import Note
from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers

def index(request):
    pesan = Note.objects.all().values()
    response = {'pesan': pesan}
    return render(request, 'lab2.html', response)

def xml(data):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(informasi):
    informasi = serializers.serialize('json', Note.objects.all())
    return HttpResponse(informasi, content_type="application/json")